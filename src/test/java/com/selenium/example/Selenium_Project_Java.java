package com.selenium.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.*;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by ili on 12/26/16.
 */
public class Selenium_Project_Java {
    private WebDriver driver;

    @BeforeClass
    public void SetUp() {
        System.setProperty("webdriver.chrome.driver", "/Users/ili/Downloads/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }


    @Test
    public void By_Text_Test() {
        driver.get("http://www.seleniumhq.org/");
        driver.findElement(new ByLinkText("Selenium Projects")).click();
        assertEquals(driver.getCurrentUrl(), "http://www.seleniumhq.org/projects/");
    }


    @Test
    public void By_Id_Test() {
        driver.get("http://www.seleniumhq.org/");
        driver.findElement(new ById("menu_download")).click();
        assertEquals(driver.getCurrentUrl(), "http://www.seleniumhq.org/download/");
    }


    @Test
    public void By_CSSSelector_Test() {
        driver.get("http://www.seleniumhq.org/");
        driver.findElement(new By.ByCssSelector("#menu_documentation>a")).click();
        assertEquals(driver.getCurrentUrl(), "http://www.seleniumhq.org/docs/");
    }

    @Test
    public void By_XPath_Test() {
        driver.get("http://www.seleniumhq.org/");
        driver.findElement(new By.ByXPath("html/body/div[3]/div[1]/ul/li[2]/a")).click();
        assertEquals(driver.getCurrentUrl(), "http://www.seleniumhq.org/support/");
    }

    @Test
    public void By_Text_Menu() {
        driver.get("http://www.seleniumhq.org/");
        driver.findElement(new ByLinkText("Overview of Selenium")).click();
        assertEquals(driver.getCurrentUrl(), "http://www.seleniumhq.org/about/");
    }
}

